package models;

import interfaces.GeometricObject;

public class Circle implements GeometricObject {
    
    protected double radius ;


    @Override
    public String toString() {
        return "Circle [radius=" + radius + "]";
    }

    public Circle() {
    }

    public Circle(double radius) {
        this.radius = radius;
    }
    // diện tích hình tròn
    public double getArea(){
        return Math.PI + Math.pow(radius, 2);
    }

        // chi vi hình tròn
    public double getPerimeter(){
        return Math.PI + (this.radius* 2);
    }

    
}
