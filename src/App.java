import models.Circle;
import models.ResizableCircle;

public class App {
    public static void main(String[] args) throws Exception {
        Circle circle = new Circle(3);
        ResizableCircle resizableCircle = new ResizableCircle(4);

        System.out.println(circle);
        System.out.println(resizableCircle);
        System.out.println("Circle: ");
        System.out.println("Diện tích hình tròn: " + circle.getArea());
        System.out.println("Chu vi hình tròn: " + circle.getPerimeter());
        System.out.println("------------------------------------");

        System.out.println("ResizableCircle: ");
        System.out.println("Diện tích hình vuông: " + resizableCircle.getArea());
        System.out.println("Chu vi hình vuông: " + resizableCircle.getPerimeter());


        resizableCircle.resize(3);
         System.out.println(resizableCircle);
    }
}
